import Male from "../user-profile/Assets/Icons/hombre.png";
import Female from "../user-profile/Assets/Icons/mujer.png";

export const datos = [
    {
        id:1,
        Nombre:'Carlos Hernán López Ruiz',
        Cargo:' Coordinador General', 
        Correo:'clopez @pedagogica.edu.co',
        Genero: <img src={Male} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:2,
        Nombre:'Ligia Lozano Cifuentes',
        Cargo:'Coordinadora Acividades Tecnológicas',
        Correo:'lclozano @pedagogica.edu.co',
        Genero: <img src={Female} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:3,
        Nombre:'Jorge Leonador Hernández Rozo',
        Cargo:'Desarrollador Web',
        Correo:'jlhernandezr @pedagogica.edu.co',
        Genero: <img src={Male} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:4,
        Nombre:'Johann Mateo Soler López',
        Cargo:'Diseñador E-learning',
        Correo:'jmsolerl@upn.edu.co',
        Genero:<img src={Male} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:5,
        Nombre:'Diana Marcela Sánchez Yáñez',
        Cargo:'Administradora Plataforma LMS',
        Correo:'dmsanchezy @pedagogica.edu.co',
        Genero: <img src={Female} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:6,
        Nombre:'Jhonny Alexander Ortegón Moreno',
        Cargo:'Gestor de Contenidos',
        Correo:'dqu_jaortegonm361 @pedagogica.edu.co',
        Genero: <img src={Male} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:7,
        Nombre:'Sandra Castañeda Valdés ',
        Cargo:'Secretaria',
        Correo:'castaned @pedagogica.edu.co',
        Genero: <img src={Female} width="180" heigth= "180" alt="logo" />,
    },
    {
        id:8,
        Nombre:'Camila Prontón Leguizamón ',
        Cargo:'Asistente Administrativa',
        Correo:'cpontonl@upn.edu.co',
        Genero: <img src={Female} width="180" heigth= "180" alt="logo" />,
    }

];
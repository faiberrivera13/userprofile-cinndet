import styled from "styled-components"
import {keyframes} from "styled-components"

//Animation Back
const BackAnimation=keyframes`
        0%{
        transform: rotate(0deg);
        }
        25%{
        transform: scale(2,8) rotate(1604deg);
        }
        50%{
        transform: scale(3,9) rotate(1643deg);
        }
        75%{
        transform: rotate(-10deg);
        }
        100%{
        transform: rotate(0deg);
        }
`

export const FullContainer = styled.div`
background: rgb(252,252,252);
background: linear-gradient(146deg, rgba(252,252,252,1) 0%, rgba(205,205,255,1) 94%);
height:auto;
width: 98%;
margin: 20px auto 0;
border-radius: 2em;
margin:30px;
`
export const NavbarContainer = styled.div`
height:15vh;
width: 100%;
background-color:transparent;
display:flex;


`
export const NavbarContainLogo = styled.div`
height:15vh;
width: 30%;
background-color:transparent;
`
export const NavbarLink = styled.div`
height:15vh;
width: 70%;
background-color:transparent;
justify-content: right;
align-content: right;
`

export const NavbarLinkEnlaces = styled.li`
        display: grid;
        justify-content:center;
        align-content: center;
        grid-template-columns: repeat(5,1fr);
        padding-left:70px;
        padding-top:5.5vh;
        font-size:1.4em;
        outline: none;
        text-decoration: none;
        color: #0000;
`

export const BodyContainer = styled.div`
        height:100%;
        width: 100%;
        display: grid;
        grid-template-rows: auto;  
        grid-gap:70px;
        grid-template-columns: repeat(3,250px);
        background-color:transparent;
        justify-content:center;
        text-align:center;
        align-items:center;
        padding-top:90px;
        padding-bottom:50px;

`


export const CardCircle = styled.div`
        width:180px;
        height:180px;
        border-radius: 50%;
        background-color: blue;
        transition: all 1.2s linear;
        grid-row-start:4;
        grid-column-start:2;
        align-items:center;
        text-align:center;
        justify-content:center;
        z-index:1;
        overflow:hidden;
`
export const ContenedorRota = styled.div`
        width:100%;
        height:50px;
        background-color:transparent;
        z-index:0;
        transform: scale(0, 0);
        grid-row-start:4;
        grid-column-start:2;
        border-radius:2em;
`
export const CardCargo = styled.div`
        transition: all 1s linear;
        width:100%;
        height:50px;
        text-align:center;
        background-color: transparent;
        justify-content:center;
        grid-row-start:2;
        grid-column-start:2;
        z-index:1;
        color:#0005ff;
        font-family:'RedRose-Medium';
        font-size:1.3em;
`
export const CardCargoTop = styled.div` 
        transition: all 1s linear;  
        width:100%;
        height:50px;
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        grid-row-start:1;
        grid-column-start:2;
        z-index:2;
        display:grid;
        grid-template-rows: 5% repeat(3, 30%) 5%;  
        border-radius: 0px 0px 15px 0px;
`

export const IconosTop = styled.div`  
        display:grid;
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:2;
        grid-template-columns: 20% repeat(4, 15%) 20%;
        grid-row-start:4;
        grid-column-start:2;
        transform: scale(0,0);
`

export const IconosFacebook = styled.div`  
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:1;
        grid-row-start:1;
        grid-column-start:2;
        transition: scale(0,0);
`

export const IconosTwitter = styled.div`  
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:1;
        grid-row-start:1;
        grid-column-start:3;
        transition: scale(0,0);
`

export const IconosWhatsapp = styled.div`  
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:1;
        grid-row-start:1;
        grid-column-start:4;
        transition: scale(0,0);
        color:transparent;
`

export const IconosLinkeding = styled.div`  
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:1;
        grid-row-start:1;
        grid-column-start:5;
        transition: scale(0,0);
`

export const CorreoTop = styled.div`
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        z-index:1;
        grid-template-rows: 100%;  
        grid-template-columns: 20% 60% 20%;
        grid-row-start:2;
        grid-column-start:2;
        transform: scale(0.8, 0.3);
        color:transparent;
        font-family:'RedRose-Medium';
`

export const CardCargoBot = styled.div`
        display:grid;
        transition: all 1s linear;
        width:100%;
        height:50px;
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        grid-row-start:5;
        grid-column-start:2;
        z-index:1;
        grid-template-rows: 5% 20% 20% 30% 20%;  
        grid-template-columns: 5% 90% 5%;
        border-radius: 15px 0px 0px 0px;
`


export const IconoBot = styled.div`
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        grid-row-start:2;
        grid-column-start:2;
        z-index:0;
`

export const NombreBot = styled.div`
        text-align:center;
        background-color: transparent;
        align-items:center;
        justify-content:center;
        grid-row-start:4;
        grid-column-start:2;
        z-index:1;
        transform: scale(0.5, 0.5s);
        color:transparent;
        font-family:'RedRose-Medium';
`


export const CardProfilecard = styled.div`
        display:grid;
        grid-template-rows: 40px 30px 40px 180px 40px;  
        grid-template-columns: 25px 180px 25px;
	width: 255px;
        height: 350px;
        background-color:transparent;
        border-radius: 2em;
        align-items:center;
        justify-content:center;
        overflow: hidden;
        transition: all .8s linear;
        transition: all 1.5s linear;
        &:hover{
                -webkit-box-shadow: -14px -6px 15px 5px rgba(0,0,0,0.27); 
                box-shadow: -14px -6px 15px 5px rgba(0,0,0,0.27);
                background-color:#fcfcfc;
        }
        &:hover ${CardCircle} {
                background: rgb(255,255,255);
                background: linear-gradient(146deg, rgba(255,255,255,1) 0%, rgba(205,205,255,1) 34%, rgba(83,79,255,1) 64%, rgba(17,14,255,1) 93%);
                transform:  scale(0, 0) rotate(360deg) translate(100px,40px);
                transition: all 1s linear;
        }
        &:hover ${CardCargo} {
                transform: scale(1.3, 1.3) translate(0,-95px);
                transition: all 0.5s linear;
                color:transparent;
        }
        &:hover ${CardCargoTop} {
                background: rgb(144,221,244);
                background: linear-gradient(180deg, rgba(144,221,244,1) 27%, rgba(112,112,255,1) 62%, rgba(5,22,254,1) 100%);
                transform: scale(1.2, 2.5) translate(0,87px) ;
                transition: all .7s linear;
        }
        &:hover ${CardCargoBot} {
                background: rgb(255,255,255);
                background: linear-gradient(180deg, rgba(255,255,255,1) 47%, rgba(144,221,244,1) 93%, rgba(144,221,244,1) 100%);
                transform: scale(1.2, 2.5) translate(0,-77px) ;
                transition: all .7s linear;
        }
        &:hover ${NombreBot}{
                transform: scale(0.8, 0.4);
                color:#0005ff;
        }
        &:hover ${CorreoTop}{
                transform: scale(0.8, 0.4);
                color:#0005ff;
        }
        &:hover ${ContenedorRota}{
                
                animation: ${BackAnimation} 5s linear infinite;
                background: rgb(252,252,252);
                background: radial-gradient(circle, rgba(252,252,252,1) 25%, rgba(237,237,253,1) 33%, rgba(225,225,254,1) 51%, rgba(9,115,186,1) 77%);
        }
        &:hover ${IconoBot}{
                transform: scale(1.8,0.8) ;
        }
        &:hover ${IconosTop}{
                transform: scale(1.4,0.6) ;
        }
`

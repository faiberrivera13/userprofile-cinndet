import {CardCargo, CardCargoBot, CardCargoTop, CardCircle, CardProfilecard,CorreoTop,IconosTop,NombreBot,IconoBot,ContenedorRota} from "./user-profile-component";
import { IconosFacebook,IconosLinkeding,IconosWhatsapp,IconosTwitter } from "./user-profile-component";
import upnlogo from "../../Imagenes/logoupn.png";
import Facebook from "../user-profile/Assets/Icons/facebook.svg";
import Whatsapp from "../user-profile/Assets/Icons/whatsapp.svg";
import Twitter from "../user-profile/Assets/Icons/twitter.svg";
import Linkedin from "../user-profile/Assets/Icons/linkedin.svg";


export const CardUserProfile = (props) => { 
        return ( 
                <CardProfilecard>
                        <ContenedorRota></ContenedorRota>
                                <CardCargoTop>
                                        <CorreoTop> {props.correo} </CorreoTop>
                                        <IconosTop>
                                                <IconosFacebook><img src={Facebook} width="12" heigth= "10" alt="logo" /></IconosFacebook>
                                                <IconosWhatsapp><img src={Whatsapp} width="12" heigth= "10" alt="logo" /></IconosWhatsapp>
                                                <IconosTwitter><img src={Twitter} width="12" heigth= "10" alt="logo" /></IconosTwitter>
                                                <IconosLinkeding><img src={Linkedin} width="12" heigth= "10" alt="logo" /></IconosLinkeding>
                                        </IconosTop>

                                </CardCargoTop>

                                <CardCircle>{props.genero}</CardCircle>
                                <CardCargo>{props.cargo}</CardCargo>

                                <CardCargoBot>
                                        <NombreBot>{props.nombre} </NombreBot>
                                        <IconoBot><img src={upnlogo} width="30" heigth= "30" alt="logo" /></IconoBot>
                                </CardCargoBot>

                </CardProfilecard>
        );
}




import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDeahRyEo_JThisVR_IoGnME4lDsuWPyeM",
  authDomain: "user-card-cinndet.firebaseapp.com",
  projectId: "user-card-cinndet",
  storageBucket: "user-card-cinndet.appspot.com",
  messagingSenderId: "777269636389",
  appId: "1:777269636389:web:9fd9a2979f25637668fecc",
  measurementId: "G-ECLVPDBEBD"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = "BBeVHiyoTJpW4cBpa78McwodBlVTQQDV0ruUlpQ9H4QrrlXzgHVmMfHJW-ssllfxpz7ieFeMfJJ-AcHs6yssvNE";
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });

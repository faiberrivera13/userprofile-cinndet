import { NavbarContainer, NavbarLinkEnlaces } from "./user-profile/user-profile-component";
import { NavbarContainLogo } from "./user-profile/user-profile-component";
import { NavbarLink } from "./user-profile/user-profile-component";
import upn from '../Imagenes/upn.svg';
import { Link } from "react-router-dom";



export const Header = () => {

    return (
        <>
        <NavbarContainer>
        <NavbarContainLogo>
                <img src={upn} width="400" heigth= "300" alt="logo" />
                </NavbarContainLogo>
                <NavbarLink>
                    <NavbarLinkEnlaces>
                        <Link to='/'>Podcast</Link>
                        <Link  to='/'>Productos</Link>
                        <Link to='/'>Servicios</Link>
                        <Link  to='/user-profile'>Equipo Cinndet</Link>
                        <Link to='/'>Moodle</Link>
                    </NavbarLinkEnlaces>
                </NavbarLink>
        </NavbarContainer>
            
        </>
        
    )
}
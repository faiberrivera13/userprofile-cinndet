import {UserProfile} from './Pages/user-profile';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'

export const App = ()=> {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path = '/' component={UserProfile}/> 
        <Route exact path = '/user-profile' component={UserProfile}/> 
        <Redirect to='/'/>
      </Switch>
    </BrowserRouter>
  );
  }
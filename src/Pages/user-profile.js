import { BodyContainer, FullContainer} from "../Components/user-profile/user-profile-component";
import { Header } from "../Components/Navbar";
import { datos } from "../Components/user-profile/data-user-profile";
import { CardUserProfile } from "../Components/user-profile/cardComponent";

export const UserProfile = ( ) => { 
        return ( 
        <FullContainer>
                <Header/>
                        <BodyContainer>
                                {datos.map((datos,index) => {
                                                return(
                                                        <div key={index}>
                                                                <CardUserProfile 
                                                                nombre={datos.Nombre}
                                                                cargo={datos.Cargo}
                                                                correo={datos.Correo}
                                                                genero={datos.Genero}
                                                                />
                                                        </div>
                                                )
                                        })
                                        }
                        </BodyContainer>
        </FullContainer>
        );
}